﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIText : MonoBehaviour
{
    private AudioSource _audioSource;

    public void Start()
    {
        _audioSource = GameObject.FindGameObjectWithTag("AudioSource").GetComponent<AudioSource>();
    }

    public void OnPointerEnter()
    {
        this.gameObject.GetComponent<Text>().fontSize += 5;
        PlaySound("Sounds/MenuClick");
    }

    public void OnPointerExit()
    {
        this.gameObject.GetComponent<Text>().fontSize -= 5;
    }

    private void PlaySound(string path)
    {
        AudioClip clip = Resources.Load(path) as AudioClip;
        _audioSource.PlayOneShot(clip);
    }
}
