
using System;
using System.Collections.Generic;

[Serializable]
public class UserHighscoreList 
{
    public List<UserHighscore> scoreList = new List<UserHighscore>();
}
