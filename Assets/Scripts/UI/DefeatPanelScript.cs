﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DefeatPanelScript : MonoBehaviour
{
   [SerializeField] private PlayerController controller;
   private string _playerName ="";

   public void GetName(string name)
   {
      _playerName = name;
   }
   public void Restart()
   {
      controller.UpdateScore(_playerName);
      Time.timeScale = 1f;
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
   }

   public void GoToMenu()
   {
      controller.UpdateScore(_playerName);
      Time.timeScale = 1f;
      SceneManager.LoadScene(0);
   }

   private void OnApplicationQuit()
   {
      if (!controller.playerDead)
         return;
      controller.UpdateScore(_playerName);
   }
}
