using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectScript : MonoBehaviour
{
    [SerializeField] private bool isNegative = false;
    [SerializeField] private GameObject sprite;

    private readonly Vector3 _scaleChange = new Vector3(0.3f,0,0);
    
    private GameObject _platform;
    private AudioSource _audioSource;
    private Collider2D _collider;

    private float _smoothTime = 2f;
    private float _yVelocity = 0.0f;
    private bool _isMoving = true;
    

    public void Start()
    {
        _platform = GameObject.FindGameObjectWithTag("Platform");
        _audioSource = GameObject.FindGameObjectWithTag("AudioSource").GetComponent<AudioSource>();
        _collider = GetComponent<Collider2D>();
    }

    private void Update()
    {
        if(!_isMoving)
            return;
        Move();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        StartCoroutine(DestroyObject());
        
        if (!other.CompareTag("Player")) return;
        
        if (isNegative)
        {
            _platform.transform.localScale -= _scaleChange;
        }
        else
        {
            _platform.transform.localScale += _scaleChange;
        }
        PlaySound("Sounds/Bloop");
    }

    private void Move()
    {
        var newPosition = Mathf.SmoothDamp(transform.position.y, -25, ref _yVelocity, _smoothTime);
        transform.position = new Vector3(transform.position.x, newPosition, transform.position.z);
        if(transform.position.y < -15 )
            Destroy(gameObject);
    }

    IEnumerator DestroyObject()
    {
        _collider.enabled = false;
        _isMoving = false;
        sprite.SetActive(false);

        yield return new WaitForSeconds(2f);
        
        Destroy(gameObject);
    }
    
    private void PlaySound(string path)
    {
        AudioClip clip = Resources.Load(path) as AudioClip;
        _audioSource.PlayOneShot(clip);
    }
}
