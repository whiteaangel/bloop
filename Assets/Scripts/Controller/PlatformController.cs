﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlatformController : MonoBehaviour
{
    [SerializeField] private float mMovementSmoothing = .01f;
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private Material material;
    
    
    private Vector3 _mVelocity = Vector3.zero;
    private Rigidbody2D _rigidbody;
    private float _speed = 2;
    private int _direction = 0;
    void Start()
    {
        Color color = new Color(
            Random.Range(0f, 1f), 
            Random.Range(0f, 1f), 
            Random.Range(0f, 1f)
        );
        material.SetColor("_GlowColor", color * 2.0f);
        GetComponent<SpriteRenderer>().color = color;
        _rigidbody = GetComponent<Rigidbody2D>();
        StartCoroutine(ChangeDirection());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Wall")) return;
        _direction *= -1;
    }

    private void Update()
    {
        if(_direction == 0)
            return;
        Move();
    }

    private void Direction()
    {
        var rand = Random.Range(0, 10);
        if (rand <= 5)
            _direction = -1;
        else
        {
            _direction = 1;
        }
        StartCoroutine(ChangeDirection());
    }

    private IEnumerator ChangeDirection()
    {
        yield return  new WaitForSeconds(3f);
        Direction();
    }
    
    private void Move()
    {
        var velocity = _rigidbody.velocity;
        Vector3 targetVelocity = new Vector2(_direction * _speed, velocity.y);
        _rigidbody.velocity = Vector3.SmoothDamp(velocity, targetVelocity, ref _mVelocity, mMovementSmoothing);
    }

    public void OnPlayerTrigger()
    {
        Vector3 scaleChange = new Vector3(0.1f,0,0);
        
        var particleSystemMain = particle.main;
        particleSystemMain.startColor = GetComponent<SpriteRenderer>().color;
        particle.Play();
        
        if(transform.localScale.x <= 0.5f)
            return;
        
        transform.localScale -= scaleChange;
        _speed += 0.1f;
    }
}
