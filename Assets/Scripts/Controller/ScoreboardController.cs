using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScoreboardController : MonoBehaviour
{
    private string _path = "";
    private readonly int _maxScoreboardEntries = 10;

    private void Start()
    {
        _path = Application.persistentDataPath + "/scoreboard.json";
    }

    public void AddEntry(UserHighscore highscore)
    {
        var highscoreList = GetSavedData();
        var scoreAdded = false;
        for (var i = 0; i < highscoreList.scoreList.Count; i++)
        {
            if (highscore.score > highscoreList.scoreList[i].score)
            {
                highscoreList.scoreList.Insert(i,highscore);
                scoreAdded = true;
                break;
            }
        }

        if (!scoreAdded && highscoreList.scoreList.Count < _maxScoreboardEntries)
        {
            highscoreList.scoreList.Add(highscore);
        }

        if (highscoreList.scoreList.Count > _maxScoreboardEntries)
        {
            highscoreList.scoreList.RemoveRange(_maxScoreboardEntries, 
                highscoreList.scoreList.Count - _maxScoreboardEntries);
        }
        SavedScore(highscoreList);
    }

    private UserHighscoreList GetSavedData()
    {
        if (!File.Exists(_path))
        {
            File.Create(_path).Dispose();
            return new UserHighscoreList();
        }
        using var stream = new StreamReader(_path);
        var json = stream.ReadToEnd();
        return JsonUtility.FromJson<UserHighscoreList>(json);
    }

    private void SavedScore(UserHighscoreList list)
    {
        using var stream = new StreamWriter(_path);
        var json = JsonUtility.ToJson(list, true);
        stream.Write(json);
    }
}
