﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneController : MonoBehaviour
{
   [SerializeField] private CanvasGroup mainPanel;
   [SerializeField] private CanvasGroup scorePanel;
   [SerializeField] private RectTransform content;
   [SerializeField] private ParticleSystem particle;
   [SerializeField] private Text soundText;
   private UserHighscoreList _highscoreList = new UserHighscoreList();
   private string _path = "";
   public void Start()
   {
      _path = Application.persistentDataPath + "/scoreboard.json";
       ReadJson("Json/Scoreboard");
       
       SetSound();
       CreateScoreboard();
       
       particle.Play();
   }
   private void ReadJson(string path)
   {
      if(!File.Exists(_path))
         return;
      using StreamReader stream = new StreamReader(_path);
      var json = stream.ReadToEnd();
      _highscoreList = JsonUtility.FromJson<UserHighscoreList>(json);
   }
  
   private void CreateScoreboard()
   {
      if(_highscoreList.scoreList.Count == 0)
         return;

      foreach (var t in _highscoreList.scoreList)
      {
         var instance = Instantiate(Resources.Load("UI/ScoreObj"), content, false) as GameObject;
         Initialize(instance, t);
      }
   }

   private void Initialize(GameObject obj, UserHighscore score)
   {
      obj.transform.GetChild(0).GetComponent<Text>().text = score.username;
      obj.transform.GetChild(1).GetComponent<Text>().text = score.score.ToString();
   }
   
   public void StartGame()
   {
      SceneManager.LoadScene(1);
   }

   public void OpenScoreboard()
   {
      if (mainPanel.alpha == 1)
      {
         particle.Stop();
         
         mainPanel.alpha = 0;
         mainPanel.interactable = false;
         mainPanel.blocksRaycasts = false;

         scorePanel.alpha = 1;
         scorePanel.interactable = true;
         scorePanel.blocksRaycasts = true;
      }
      else 
      {
         particle.Play();
         
         mainPanel.alpha = 1;
         mainPanel.interactable = true;
         mainPanel.blocksRaycasts = true;
         
         scorePanel.alpha = 0;
         scorePanel.interactable = false;
         scorePanel.blocksRaycasts = false;
      }
   }

   public void SoundOff()
   {
      if (PlayerPrefs.GetInt("Sound") == 1 || 
          PlayerPrefs.GetInt("Sound") == 0)
      {
         PlayerPrefs.SetInt("Sound", 2);
         soundText.text = "Sound off";
      }
      else 
      {
         PlayerPrefs.SetInt("Sound", 1);
         soundText.text = "Sound on";
      }
      
      PlayerPrefs.Save();
      SetSound();
   }

   private void SetSound()
   {
      var sound = PlayerPrefs.GetInt("Sound");
      if (sound == 0)
         sound = 1;
      GetComponent<AudioSource>().volume = sound == 1 ? 0.5f : 0f;
      soundText.text = sound == 1 ? "Sound on" : "Sound off";
   }

   public void Exit()
   {
      Application.Quit();
   }
}
