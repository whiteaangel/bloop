using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class MainSceneController : MonoBehaviour
{
    [SerializeField] private CanvasGroup tutorialPanel;
    private void Start()
    {
        SetSound();
        
        var firstStart = PlayerPrefs.GetInt("IsFirstLoad");
        if (firstStart == 1)
        {
            StartCoroutine(SpawnEffect());
            return;
        }
        
        tutorialPanel.alpha = 1;
        tutorialPanel.blocksRaycasts = true;
        tutorialPanel.interactable = true;
        Time.timeScale = 0;
    }

    public void CloseTutorialPanel()
    {
        tutorialPanel.alpha = 0;
        tutorialPanel.blocksRaycasts = false;
        tutorialPanel.interactable = false;
        
        PlayerPrefs.SetInt("IsFirstLoad",1);
        PlayerPrefs.Save();
        
        StartCoroutine(SpawnEffect());
        Time.timeScale = 1;
    }
    
    private void SetSound()
    {
        var sound = PlayerPrefs.GetInt("Sound");
        Debug.Log(sound);
        GetComponent<AudioSource>().volume = sound == 1 ? 0.5f : 0f;
    }

    private IEnumerator SpawnEffect()
    {
        var randTime = Random.Range(2f, 4f);
        var randEffect = Random.Range(0, 10);
        var randomPositionX = Random.Range(-4.5f, 4.5f);

        yield return new WaitForSeconds(randTime);
        var path = randEffect < 5 ? "PlatformUP" : "PlatformDown";
        Instantiate(Resources.Load(path), 
            new Vector3(randomPositionX, 10.5f, 0), quaternion.identity);
        StartCoroutine(SpawnEffect());
    }
}
