﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Animator animator;
    [SerializeField] private float mMovementSmoothing = 0f;
    [SerializeField] private Transform point;
    [SerializeField] private GameObject scoreText;
    [SerializeField] private GameObject complimentText;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private Material material;
    [SerializeField] private ScoreboardController scoreboardController;

    public float range = 0.5f;
    public LayerMask layer;
    public bool playerDead = false;
    
    private int _direction = 0;
    private readonly int _speed = 3;
    private float _nextTime = 0f;

    private int _count = 0;
    private bool _isDown = false;
    
    private Rigidbody2D _rigidbody;	
    private Vector3 _mVelocity = Vector3.zero;

    private readonly string[] _complimentArr = {"Awesome!","Great!","Wonderful","Wow!",
        "Amazing","Cool!", "Nice" };
    
    void Start()
    {
        Color color = new Color(
            Random.Range(0f, 1f), 
            Random.Range(0f, 1f), 
            Random.Range(0f, 1f)
        );
        material.SetColor("_GlowColor", color * 2.0f);
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = color;
        
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.freezeRotation = true;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("WallDown")) return;
        PlaySound("Sounds/Hurt");
        StartCoroutine(WhaitForDead());
    }

    IEnumerator WhaitForDead()
    {
        playerDead = true;
        
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
        
        var defeatPanel = GameObject.FindGameObjectWithTag("DefeatPanel").GetComponent<CanvasGroup>();
        defeatPanel.alpha = 1;
        defeatPanel.interactable = true;
        defeatPanel.blocksRaycasts = true;

        defeatPanel.transform.GetChild(1).GetComponent<Text>().text = "Your score  is:   " + _count;
        
        Time.timeScale = 0f;
    }
    
    public void UpdateScore(string userName)
    {
        if (userName == "")
            userName = "Player";
        var newScore = new UserHighscore {username = userName, score = _count};
        scoreboardController.AddEntry(newScore);
    }
    void Update()
    {
        MovingPlayer();
        OnPlatformTouch();
    }

    private void MovingPlayer()
    {
        if (Input.touchCount <= 0) return;
        var touch = Input.GetTouch(0);
        
        Vector3 position = cam.ScreenToWorldPoint(touch.position);

        if (position.x > 0 && position.y > -6)
            _direction = 1;
        else if(position.x < 0 && position.y > -6)
        {
            _direction = -1;
        }

        if (position.y < -6 && !_isDown)
        {
            _rigidbody.gravityScale = 3f;
            _isDown = true;
        }
        
        var velocity = _rigidbody.velocity;
        Vector3 targetVelocity = new Vector2(_direction * _speed, velocity.y);
        _rigidbody.velocity = Vector3.SmoothDamp(velocity, targetVelocity, ref _mVelocity, mMovementSmoothing);
    }

    private void OnPlatformTouch()
    {
        Collider2D[] hit;
        hit = Physics2D.OverlapCircleAll(point.position, range, layer);

        if (hit.Length == 0)
            return;
        
        if (!(Time.time > _nextTime)) return;
        
        if (_isDown)
        {
            _count += 2;
            StartCoroutine(ReturnGravity());
        }
        else
        {
            _count++;
        }
        
        animator.SetTrigger("Jump");
            
        PlaySound("Sounds/Jump");
        StartCoroutine(UpdateText());
            
        foreach (var obj in hit)
        {
            obj.GetComponent<PlatformController>().OnPlayerTrigger();
        }
            
        _nextTime = Time.time + 0.5f;
    }

    private IEnumerator ReturnGravity()
    {
        var rand = Random.Range(0, _complimentArr.Length - 1);
        
        var scaleRand = Random.Range(1.5f, 2f);
        var rotRand = Random.Range(-20,20);
        
        complimentText.GetComponent<Text>().text = _complimentArr[rand];
        
        complimentText.transform.localScale = new Vector3(scaleRand,scaleRand,0);
        complimentText.transform.Rotate(new Vector3(0,0,rotRand));
        
        yield return new WaitForSeconds(0.3f);
        
        _rigidbody.gravityScale = 1f;
        _isDown = false;

        yield return new WaitForSeconds(0.5f);
        
        complimentText.GetComponent<Text>().text = "";
        complimentText.transform.localScale = new Vector3(1,1,1);
        complimentText.transform.rotation = new Quaternion(0,0,0,1);
    }
    
    
    private void PlaySound(string path)
    {
        AudioClip clip = Resources.Load(path) as AudioClip;
        audioSource.PlayOneShot(clip);
    }
    
    private IEnumerator UpdateText()
    {
        Vector3 scale = scoreText.transform.localScale;
        Quaternion rotation = scoreText.transform.rotation;

        var rand = Random.Range(1.2f, 2f);
        var randRot = Random.Range(-45,45);

        scoreText.GetComponent<Text>().text = "x" + _count;
        scoreText.transform.localScale = new Vector3(rand,rand,0);
        scoreText.transform.Rotate(new Vector3(0,0,randRot));

        yield return  new WaitForSeconds(1f);
        scoreText.transform.localScale = scale;
        scoreText.transform.rotation = rotation;
    }
    private void OnDrawGizmos()
    {
         Gizmos.DrawWireSphere(point.position, range);
    }
}
